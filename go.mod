module auth

go 1.14

require (
	github.com/Unleash/unleash-client-go/v3 v3.1.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.12
	github.com/lib/pq v1.5.2 // indirect
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
)
